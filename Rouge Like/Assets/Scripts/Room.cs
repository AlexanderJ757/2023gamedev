using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Room : MonoBehaviour
{
    //Reference for the door objects
    [Header("Door Objects")]
    public Transform northDoor;
    public Transform southDoor;
    public Transform eastDoor;
    public Transform westDoor;
    //Reference for the wall objects
    [Header("Wall Objects")]
    public Transform northWall;
    public Transform southWall;
    public Transform eastWall;
    public Transform westWall;
    // how many tiles are there in the room
    [Header("Room Size")]
    public int insideWidth;
    public int insideheight;
    //Object to instanstiate
    [Header("Room Prefabs")]
    public GameObject enemyPrefabs;
    public GameObject coinPrefabs;
    public GameObject healthPrefabs;
    public GameObject keyPrefabs;
    public GameObject exitDoorPrefabs;
    // List of positions to avoid instantiating new objects on top of olf
    private List<Vector3> usedPosition = new List<Vector3>();

    public void GenerateInterior()
    {

    }

    public void SpawnPrefab(GameObject prefab, int min = 0, int max = 0)
    {
        // Spawn
    }
    
    // Start is called before the first frame update
    
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
